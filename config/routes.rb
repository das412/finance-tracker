Rails.application.routes.draw do
  get 'user_stocks/create'
  devise_for :users, :controllers => { :registrations => "user/registrations" }
  get 'my_portfolio', to: "users#my_portfolio"
  get 'my_friends', to: "users#my_friends"
  post 'add_friend', to: "users#add_friend"
  get 'search_stocks', to: "stocks#search"
  get 'search_friends', to: "users#search"
  root 'welcome#index'
  resources :user_stocks, only: [:create,:destroy]
  resources :users, only: [:show]
  resources :friendships
end
