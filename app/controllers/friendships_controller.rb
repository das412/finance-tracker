class FriendshipsController < ApplicationController

  def destroy
    # why doesn `.where` work?
    # wrong number of arguments (given 0, expected 1)
    # @friendship = current_user.friendships.where(friend_id: params[:id].first)
    @friendship = current_user.friendships.find_by(friend_id: params[:id].first)
    @friendship.destroy
    flash[:notice] = "Friend was successfully removed"
    redirect_to my_friends_path
  end

end
